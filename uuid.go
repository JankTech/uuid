package uuid

import (
	"bytes"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"slices"
	"strings"

	"github.com/google/uuid"
)

var Nil UUID

type UUID struct {
	uuid.UUID
}

// Alias for NewRandom
func New() UUID {
	return UUID{UUID: uuid.New()}
}

// Gets a new random UUID
func NewRandom() (UUID, error) {
	u, err := uuid.NewRandom()
	return UUID{UUID: u}, err
}

func Must(u UUID, err error) UUID {
	if err != nil {
		panic(err)
	}
	return u
}

// Gets a new random (v4) UUID as a string, and panics if there is an error.
func NewString() string {
	return uuid.NewString()
}

// NewV3 implements a simple namespacing and byte concatenation wrapper to the MD5 UUID format
func NewV3(namespace string, hashData [][]byte) UUID {
	return UUID{UUID: uuid.NewMD5(getUUIDNameSpace(namespace), joinByteArrays(hashData))}
}

// NewV5 implements a simple namespacing and byte concatenation wrapper to the SHA1 UUID format
func NewV5(namespace string, hashData [][]byte) UUID {
	return UUID{UUID: uuid.NewSHA1(getUUIDNameSpace(namespace), joinByteArrays(hashData))}
}

func joinByteArrays(b [][]byte) []byte {
	return bytes.Join(b, []byte{0x1f})
}

func getUUIDNameSpace(key string) uuid.UUID {
	return uuid.NewMD5(uuid.Nil, []byte(key))
}

// Validate returns an error if s is not a properly formatted UUID in one of the following formats:
//
//	xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
//	urn:uuid:xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
//	xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//	{xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx}
//	xxxxxxxxxxxxxxxxxxxxxx
//
// It returns an error if the format is invalid, otherwise nil.
func Validate(s string) error {
	if len(s) == 22 {
		bytes, err := decodeBase64String(s)
		if err != nil {
			return fmt.Errorf("invalid base64 format: %s", s)
		}
		s = hex.EncodeToString(bytes)
	}
	return uuid.Validate(s)
}

func (u UUID) ToStringBase64URL() string {
	return base64.RawURLEncoding.EncodeToString(u.UUID[:])
}

func (u UUID) ToStringBase64Std() string {
	return base64.RawStdEncoding.EncodeToString(u.UUID[:])
}

func (u UUID) ToStringBase16() string {
	return fmt.Sprintf("%x", u.UUID[:])
}

func (u UUID) ToStringCanonical() string {
	return u.String()
}

func (u UUID) ToStringGUID() string {
	return fmt.Sprintf("{%s}", u.ToStringCanonical())
}

func (u UUID) ToStringURN() string {
	return fmt.Sprintf("urn:uuid:%s", u.ToStringCanonical())
}

func (uuid *UUID) UnmarshalJSON(j []byte) error {
	u, err := ParseBytes(j[1 : len(j)-1])
	if err != nil {
		return err
	}

	*uuid = u
	return nil
}

// Parse decodes s into a UUID or returns an error if it cannot be parsed.
// Both the standard UUID forms defined in RFC 4122 (xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx and urn:uuid:xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx) are decoded.
// In addition, Parse accepts non-standard strings such as:
//
//	base64 (standard or urlsafe) encoding xxxxxxxxxxxxxxxxxxxxxx
//	the raw hex encoding xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//	and 38 byte "Microsoft style" encodings, e.g. {xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx}
//
// Only the middle 36 bytes are examined in the latter case.
// Parse should not be used to validate strings as it parses non-standard encodings as indicated above.
func Parse(s string) (UUID, error) {
	// Strings of length 22 might be base64 encoded UUIDs
	if len(s) == 22 {
		bytes, err := decodeBase64String(s)
		if err != nil {
			return UUID{UUID: uuid.Nil}, err
		}

		u, err := uuid.FromBytes(bytes)
		if err != nil {
			return UUID{uuid.Nil}, err
		}

		return UUID{UUID: u}, nil
	}

	// Else parse using google's lib
	u, err := uuid.Parse(s)
	if err != nil {
		return UUID{uuid.Nil}, err
	}

	return UUID{UUID: u}, nil
}

func ParseBytes(b []byte) (UUID, error) {
	if len(b) == 22 {
		bytes, err := decodeBase64Bytes(b)
		if err != nil {
			return UUID{UUID: uuid.Nil}, err
		}

		u, err := uuid.FromBytes(bytes)
		if err != nil {
			return UUID{uuid.Nil}, err
		}

		return UUID{UUID: u}, nil
	}

	u, err := uuid.ParseBytes(b)
	return UUID{UUID: u}, err
}

func FromByteArray(b [16]byte) UUID {
	return UUID{uuid.UUID(b)}
}

// Scan implements sql.Scanner so UUIDs can be read from databases transparently.
// Currently, database types that map to string and []byte are supported. Please
// consult database-specific driver documentation for matching types.
func (uuid *UUID) Scan(src interface{}) error {
	switch src := src.(type) {
	case nil:
		return nil

	case string:
		// if an empty UUID comes from a table, we return a null UUID
		if src == "" {
			return nil
		}

		// see Parse for required string formats
		u, err := Parse(src)
		if err != nil {
			return fmt.Errorf("Scan: %v", err)
		}

		*uuid = u
	case [16]byte:
		u := FromByteArray(src)
		*uuid = u
	default:
		return uuid.Scan(src)
	}

	return nil
}

func decodeBase64String(s string) ([]byte, error) {
	// Check if we have a URL encoded base64 string.
	if strings.Contains(s, "-") || strings.Contains(s, "_") {
		return base64.RawURLEncoding.DecodeString(s)
	}

	// If '-' and '_' are not included then the decode is the same anyway.
	return base64.RawStdEncoding.DecodeString(s)
}

func decodeBase64Bytes(b []byte) ([]byte, error) {
	dst := make([]byte, 16)
	if slices.Contains(b, 45) || slices.Contains(b, 95) {
		_, err := base64.RawURLEncoding.Decode(dst, b)
		return dst, err
	}

	_, err := base64.RawStdEncoding.Decode(dst, b)
	return dst, err
}
