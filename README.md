# uuid

This packages is a convenience wrapper for [google's UUID package](https://github.com/google/uuid).

## Additional Methods

Other than the base methods provided by google's package, this package provides the following formatting APIs:

```go
func (u UUID) ToStringCanonical() string // "5f23abf5-b944-45f2-b5ac-a69629cff8be" // Alias of String()
func (u UUID) ToStringBase64URL() string // "XyOr9blERfK1rKaWKc_4vg"
func (u UUID) ToStringBase64Std() string // "XyOr9blERfK1rKaWKc/4vg"
func (u UUID) ToStringBase16() string    // "5f23abf5b94445f2b5aca69629cff8be"
func (u UUID) ToStringGUID() string      // "{5f23abf5-b944-45f2-b5ac-a69629cff8be}"
func (u UUID) ToStringURN() string       // "urn:uuid:5f23abf5-b944-45f2-b5ac-a69629cff8be"
```

The `Parse` function has also been extended to accept base64URL and base64Std encoded, 22 length string formats:

```go
u, err := uuid.Parse("XyOr9blERfK1rKaWKc/4vg")
// error handling ...
fmt.Println(u.String()) // "5f23abf5-b944-45f2-b5ac-a69629cff8be"
```

## Installation

```sh
go get gitlab.com/JankTech/uuid
```

## Documentation

[![Go Reference](https://pkg.go.dev/badge/gitlab.com/JankTech/uuid.svg)](https://pkg.go.dev/gitlab.com/JankTech/uuid)

Full `go doc` style documentation for the package can be viewed online without
installing this package by using the GoDoc site here: 
http://pkg.go.dev/gitlab.com/JankTech/uuid
