package uuid_test

import (
	"encoding/json"
	"testing"

	"gitlab.com/JankTech/uuid"
)

type uuidTestCase struct {
	name      string
	canonical string
	base64URL string
	base64Std string
	base16    string
	guid      string
	urn       string
}

var uuidTestCases = []uuidTestCase{
	{
		name:      "with base64 slash",
		canonical: "5f23abf5-b944-45f2-b5ac-a69629cff8be",
		base64URL: "XyOr9blERfK1rKaWKc_4vg",
		base64Std: "XyOr9blERfK1rKaWKc/4vg",
		base16:    "5f23abf5b94445f2b5aca69629cff8be",
		guid:      "{5f23abf5-b944-45f2-b5ac-a69629cff8be}",
		urn:       "urn:uuid:5f23abf5-b944-45f2-b5ac-a69629cff8be",
	},
	{
		name:      "with base64 plus",
		canonical: "be9fae66-3695-49dc-aaea-711cae79f5f9",
		base64URL: "vp-uZjaVSdyq6nEcrnn1-Q",
		base64Std: "vp+uZjaVSdyq6nEcrnn1+Q",
		base16:    "be9fae66369549dcaaea711cae79f5f9",
		guid:      "{be9fae66-3695-49dc-aaea-711cae79f5f9}",
		urn:       "urn:uuid:be9fae66-3695-49dc-aaea-711cae79f5f9",
	},
	{
		name:      "with no base64 special chars",
		canonical: "7f36b325-501b-4acb-9e07-4e368162e5b2",
		base64URL: "fzazJVAbSsueB042gWLlsg",
		base64Std: "fzazJVAbSsueB042gWLlsg",
		base16:    "7f36b325501b4acb9e074e368162e5b2",
		guid:      "{7f36b325-501b-4acb-9e07-4e368162e5b2}",
		urn:       "urn:uuid:7f36b325-501b-4acb-9e07-4e368162e5b2",
	},
}

func TestUUID(t *testing.T) {
	t.Run("format", func(t *testing.T) {
		t.Parallel()
		for _, testCase := range uuidTestCases {
			t.Run(testCase.name, testUUIDFormatsCorrectly(testCase))
		}
	})

	t.Run("parse", func(t *testing.T) {
		t.Parallel()
		for _, testCase := range uuidTestCases {
			t.Run(testCase.name, testUUIDParsesCorrectly(testCase))
		}
	})

	t.Run("parseBytes", func(t *testing.T) {
		t.Parallel()
		for _, testCase := range uuidTestCases {
			t.Run(testCase.name, testUUIDParsesBytesCorrectly(testCase))
		}
	})

	t.Run("parseJSON", func(t *testing.T) {
		t.Parallel()
		for _, testCase := range uuidTestCases {
			t.Run(testCase.name, testUUIDParsesAsJSONCorrectly(testCase))
		}
	})
}

func testUUIDParsesAsJSONCorrectly(testCase uuidTestCase) func(t *testing.T) {
	return func(t *testing.T) {
		t.Parallel()
		inputs := map[string]string{
			"base64URL": testCase.base64URL,
			"base64Std": testCase.base64Std,
			"base16":    testCase.base16,
			"guid":      testCase.guid,
			"urn":       testCase.urn,
			"canonical": testCase.canonical,
		}

		for name, input := range inputs {
			t.Run(name, func(t *testing.T) {
				t.Parallel()

				j := struct {
					ID uuid.UUID `json:"id"`
				}{}

				err := json.Unmarshal([]byte(`{"id": "`+input+`"}`), &j)
				if err != nil {
					t.Fatal(err)
				}

				actual := j.ID.String()
				if actual != testCase.canonical {
					t.Fatalf("incorrect parsing of JSON UUID %s, expected %s, got %s", input, testCase.canonical, actual)
				}
			})
		}
	}
}

func testUUIDParsesBytesCorrectly(testCase uuidTestCase) func(t *testing.T) {
	return func(t *testing.T) {
		t.Parallel()

		u, err := uuid.ParseBytes([]byte(testCase.base64URL))
		if err != nil {
			t.Fatal(err)
		}

		actual := u.String()
		if actual != testCase.canonical {
			t.Fatalf("incorrect parsing of bytes UUID %s, expected %s, got %s", testCase.base64URL, testCase.canonical, actual)
		}

		u, err = uuid.ParseBytes([]byte(testCase.base64Std))
		if err != nil {
			t.Fatal(err)
		}

		actual = u.String()
		if actual != testCase.canonical {
			t.Fatalf("incorrect parsing of bytes UUID %s, expected %s, got %s", testCase.base64URL, testCase.canonical, actual)
		}
	}
}

func testUUIDFormatsCorrectly(testCase uuidTestCase) func(t *testing.T) {
	return func(t *testing.T) {
		t.Parallel()

		u, err := uuid.Parse(testCase.canonical)
		if err != nil {
			t.Fatal(err)
		}

		base64URL := u.ToStringBase64URL()
		if testCase.base64URL != base64URL {
			t.Errorf("incorrect %s formatting of UUID, expected %s, got %s", "base64URL", testCase.base64URL, base64URL)
		}

		base64Std := u.ToStringBase64Std()
		if testCase.base64Std != base64Std {
			t.Errorf("incorrect %s formatting of UUID, expected %s, got %s", "base64Std", testCase.base64Std, base64Std)
		}

		base16 := u.ToStringBase16()
		if testCase.base16 != base16 {
			t.Errorf("incorrect %s formatting of UUID, expected %s, got %s", "base16", testCase.base16, base16)
		}

		guid := u.ToStringGUID()
		if testCase.guid != guid {
			t.Errorf("incorrect %s formatting of UUID, expected %s, got %s", "guid", testCase.guid, guid)
		}

		urn := u.ToStringURN()
		if testCase.urn != urn {
			t.Errorf("incorrect %s formatting of UUID, expected %s, got %s", "urn", testCase.urn, urn)
		}

		canonical := u.ToStringCanonical()
		if testCase.canonical != canonical {
			t.Errorf("incorrect %s formatting of UUID, expected %s, got %s", "canonical", testCase.canonical, canonical)
		}
	}
}

func testUUIDParsesCorrectly(testCase uuidTestCase) func(t *testing.T) {
	return func(t *testing.T) {
		t.Parallel()
		inputs := map[string]string{
			"base64URL": testCase.base64URL,
			"base64Std": testCase.base64Std,
			"base16":    testCase.base16,
			"guid":      testCase.guid,
			"urn":       testCase.urn,
			"canonical": testCase.canonical,
		}

		for name, input := range inputs {
			t.Run(name, func(t *testing.T) {
				t.Parallel()

				u, err := uuid.Parse(input)
				if err != nil {
					t.Fatal(err)
				}

				actual := u.String()
				if actual != testCase.canonical {
					t.Fatalf("incorrect parsing of string UUID %s, expected %s, got %s", input, testCase.canonical, actual)
				}
			})
		}
	}
}
